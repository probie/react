var React = require('react');

var Header = React.createClass({
    getInitialState: function() {
        return {
            text: ''
        }
    },
    render: function() {
        return <div className="input-group">
            <input
                type="text"
                className="form-control"
                value={this.state.text}
                onChange={this.handleInputChange}
            />
            <span className="input-group-btn">
                <button
                    className="btn btn-primary"
                    onClick={this.handleClick}
                    type="button">
                    Add Item
                </button>
            </span>
        </div>
    },
    handleClick: function() {
        console.log(this.state.text);
        if(this.state.text !== ''){
            this.props.itemsStore.push({
                text: this.state.text,
                done: false
            });
        } else {
            alert('Please do not add empty data.');
        }
        this.setState({text: ''});
    },
    handleInputChange: function(event) {
        this.setState({text: event.target.value});
    }
});

module.exports = Header;
